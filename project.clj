(defproject jslib-tests "0.1.0-SNAPSHOT"
  :description "Starter application for ClojureScript web application!"
  :url "http://example.com/"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.8.40"]
                 [ring "1.4.0"]]

  :plugins [[lein-cljsbuild "1.1.3"]
            [lein-ring "0.9.7"]
            [lein-figwheel "0.5.2"]]

  :hooks [leiningen.cljsbuild]
  :source-paths ["src/clj"]

  :cljsbuild {:builds {:main {:source-paths ["src/cljs"]
                              :jar true
                              :figwheel true
                              :compiler {:output-to "resources/public/js/main.js"
                                         :optimizations :none
                                         :output-dir "resources/public/js/out"
                                         :source-map true
                                         :asset-path "js/out"
                                         :main jslib-tests.client
                                         :pretty-print true}}}}
  :min-lein-version "2.0.0"
  :uberjar-name "jslib-tests.jar"
  :main jslib-tests.server
  :aot [jslib-tests.server]
  :ring {:handler jslib-tests.server/app})
